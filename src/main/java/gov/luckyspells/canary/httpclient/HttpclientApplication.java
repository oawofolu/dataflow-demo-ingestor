package gov.luckyspells.canary.httpclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.app.httpclient.processor.HttpclientProcessorConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(HttpclientProcessorConfiguration.class)
public class HttpclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpclientApplication.class, args);
	}
}
